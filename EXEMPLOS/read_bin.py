# -*- coding: utf-8 -*-

import numpy as np
from scipy.io import FortranFile
from decimal import *

# coordenadas
resol = 0.54

lons = [ -55.637 + (resol * i) for i in range(109)]
lons = [ float(Decimal("%.2f" % elem)) for elem in lons]

lats = [ -21.397, -20.893, -20.387, -19.880, -19.371, -18.861, -18.349,
         -17.835, -17.320, -16.803, -16.285, -15.766, -15.246, -14.724,
         -14.200, -13.676, -13.150, -12.624, -12.096, -11.567, -11.037,
         -10.506,  -9.975,  -9.442,  -8.909,  -8.375,  -7.840,  -7.304,
          -6.768,  -6.231,  -5.694,  -5.156,  -4.617,  -4.079,  -3.539,
          -3.000,  -2.460,  -1.920,  -1.380,  -0.840,  -0.300,   0.241,
           0.781,   1.321,   1.861,   2.401,   2.941,   3.480,   4.019,
           4.558,   5.097,   5.635,   6.172,   6.709,   7.245,   7.781,
           8.316,   8.850,   9.384,   9.916,  10.448,  10.979,  11.509,
          12.038,  12.566,  13.093,  13.618,  14.143,  14.666,  15.188,
          15.709,  16.229 ]

nday = 30
nlat = len(lats)
nlon = len(lons)

bin_file = 'plev.198811.DAILY.PER11.51'
bin_data = FortranFile(bin_file, 'r')

pcp = np.full((nday, nlat, nlon), np.nan)
pcpc = np.full((nday, nlat, nlon), np.nan)

for i in range(nday):
    aux_pcp = (bin_data.read_reals(dtype='float32')).reshape(nlat, nlon)
    pcp[i, ...] = aux_pcp
    aux_pcpc = (bin_data.read_reals(dtype='float32')).reshape(nlat, nlon)
    pcpc[i, ...] = aux_pcpc
