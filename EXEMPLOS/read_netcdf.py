# -*- coding: utf-8 -*-

from netCDF4 import Dataset

nc_file = 'cmap.precip.mon.ltm.nc'
nc_data = Dataset(nc_file, mode='r')

print(nc_data)
print(nc_data.variables)

lons = nc_data.variables['lon'][:]
lats = nc_data.variables['lat'][:]
pcp = nc_data.variables['precip'][:]
pcp_units = nc_data.variables['precip'].units

print('Lat shape:', lats.shape)
print('Lon shape', lons.shape)
print('Precip shape:', pcp.shape)
print('Precip type:', type(pcp))
print('Previp units:', pcp_units)

nc_data.close()
