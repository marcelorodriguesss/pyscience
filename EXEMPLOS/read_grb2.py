# -*- coding: utf-8 -*-

import pygrib

grb_file = 'prate.01.2016102900.daily.grb2'
grb_data = pygrib.open(grb_file)

# Verificando a estrutura do arquivo
for grb in grb_data:
    print(grb)

grb_pcp = grb_data.select(name='Precipitation rate')[0]
pcp = grb_pcp.values
lats, lons = grb_pcp.latlons()  # irregular grid

print('Precip shape:', pcp.shape)
print('Precip type:', type(pcp))
print('Lat shape:', lats.shape)
print('Lon shape', lons.shape)

grb_data.close()
